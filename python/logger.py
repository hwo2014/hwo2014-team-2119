import logging

class Logger:

    def __init__ (self, INFO, THINK):
        self.INFO = INFO
        self.THINK = THINK
        logging.basicConfig(filename='bot2.log', level=logging.INFO, format='%(asctime)s %(message)s')
        logging.info('Starting Race')

    def mainLog(self):
        #testando se as informacoes do carro estao corretas
        #if INFO.gameTick % 10 == 0:
        #    print "**************************************"
        print "gameTick = " + str(self.INFO.gameTick), " Throttle = " + str(self.THINK.getThrottle()), " velocity = " + str(self.INFO.myCar.velocity), " aceleration = " + str(self.INFO.myCar.aceleration), " piece = " + str(self.INFO.myCar.pieceIndex), " angle = " + str(self.INFO.myCar.angle) + " tb = " + str(self.THINK.tb) + " inPDist = " + str(self.INFO.myCar.inPieceDistance)
        #print "Proxima curva: " + str(INFO.get_last_not_curve_index())
        logging.info( "gameTick = " + str(self.INFO.gameTick) + " Throttle = " + str(self.THINK.getThrottle()) +  " velocity = " + str(self.INFO.myCar.velocity) + " aceleration = " + str(self.INFO.myCar.aceleration) + " piece = " + str(self.INFO.myCar.pieceIndex) + " angle = " + str(self.INFO.myCar.angle) + " tb = " + str(self.THINK.tb))
        logging.info("Proxima curva: " + str(self.INFO.get_last_not_curve_index()) )
        #    print "angle = " + str(INFO.myCar.angle)
        #    print "pieceIndex = " + str(INFO.myCar.pieceIndex)
        #    print "inPieceDistance = " + str(INFO.myCar.inPieceDistance)
        #    print "startLaneIndex = " + str(INFO.myCar.startLaneIndex)
        #    print "endLaneIndex = " + str(INFO.myCar.endLaneIndex)
        #    print "lap " + str(INFO.myCar.lap)

    def commandLineError(self):
        logging.basicConfig(filename='bot2.log', level=logging.INFO, format='%(asctime)s %(message)s')
        logging.info('Starting Race')

    def starting(self):
        logging.info("Connecting with parameters:")
        logging.info("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))