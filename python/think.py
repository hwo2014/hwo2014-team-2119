import math

class Think:
    throttle = 0.0
    tb = 0
    
    def __init__(self, INFOreceived):
        self.INFO = INFOreceived
    
    def getThrottle(self):
        return self.throttle
       
    #logica de myCar
    def calculateThrottle(self):
        # crashed ou comeco da corrida
        if self.INFO.myCar.velocity == 0.0:
            self.throttle = 1.0
        else:
            #Se estou na reta
            if self.INFO.TrackList[self.INFO.myCar.pieceIndex].radius == 0.0:
                self.throttle = self.getBestThrottleInRect (self.INFO.myCar.pieceIndex, self.INFO.get_last_not_curve_index())
            #Se estou na curva
            else:
                self.throttle = self.getBestThrottleInCurve(self.INFO.myCar.pieceIndex, self.INFO.get_last_not_curve_index())
                print "Throttle na curva: " + str(self.throttle)
            
    def getBestThrottleInRect(self, pieceIndex, finalPieceIndex):
        xc = 0
        nextCurveIndex = (self.INFO.get_last_not_curve_index() + 1)%self.INFO.totalPieces
        print "next curve index = " +  str(nextCurveIndex)
        vFinal = 10*(self.getMaxThrottleInCurve(self.INFO.TrackList[nextCurveIndex].originalRadius))
        print "vFinal = " + str(vFinal)
        if pieceIndex > finalPieceIndex:
            for i in range(pieceIndex, self.INFO.totalPieces):
                if i == pieceIndex:
                    xc = xc + self.INFO.TrackList[pieceIndex].length - self.INFO.myCar.inPieceDistance
                else:
                    xc = xc + self.INFO.TrackList[pieceIndex].length
            for i in range(0, finalPieceIndex+1):
                if i == pieceIndex:
                    xc = xc + self.INFO.TrackList[pieceIndex].length - self.INFO.myCar.inPieceDistance
                else:
                    xc = xc + self.INFO.TrackList[pieceIndex].length
        else:
            for i in range(pieceIndex, finalPieceIndex + 1):
                if i == pieceIndex:
                    xc = xc + self.INFO.TrackList[pieceIndex].length - self.INFO.myCar.inPieceDistance
                else:
                    xc = xc + self.INFO.TrackList[pieceIndex].length
        
        denominator = 1.0
        if( self.INFO.myCar.power > 0 ):
            denominator = self.INFO.myCar.power
        
        self.tb = ((self.INFO.friction/denominator)*((xc)+(vFinal/self.INFO.friction)-(self.INFO.myCar.velocity/self.INFO.friction)))
        
        if( self.INFO.turboFactor <= 1.0 ):
            if( self.tb >= 1.0 + self.INFO.nSwitchLane or ( self.INFO.myCar.lap == 2 and self.INFO.myCar.pieceIndex > self.INFO.get_last_not_curve_index())):
                return 1.0
            elif self.tb > 0.0 and self.tb < 1.0:
                return 0.98*self.tb
            else:
                return 0.0
        else:
            if( self.tb >= self.INFO.turboFactor + self.INFO.nSwitchLane or ( self.INFO.myCar.lap == 2 and self.INFO.myCar.pieceIndex > self.INFO.get_last_not_curve_index())):
                return 1.0
            else:
                return 0.0

    def getBestThrottleInCurve (self, pieceIndex, finalPieceIndex):
        radius = self.INFO.TrackList[pieceIndex].originalRadius
        angle = self.INFO.TrackList[pieceIndex].angle
        return self.getMaxThrottleInCurve(radius)

    def getMaxThrottleInCurve (self, radius):
        cte = 0.42
        return math.sqrt(cte*radius)/10.0

    def getBestLane (self):
        pieceIndex = self.INFO.myCar.pieceIndex
        totalPieces = self.INFO.totalPieces
        if self.INFO.rightOrLeft[(pieceIndex + 1)%totalPieces] != 0:
            if (self.INFO.rightOrLeft[(pieceIndex + 1)%totalPieces] > 0 and (pieceIndex + 1)%totalPieces != self.INFO.toSwitchJustOnce):
                return "Right"
                self.INFO.toSwitchJustOnce = (pieceIndex + 1)%self.INFO.totalPieces
                logging.info ("Right")
            elif (self.INFO.rightOrLeft[(pieceIndex + 1)%totalPieces] < 0 and (pieceIndex + 1)%totalPieces != self.INFO.toSwitchJustOnce):
                return "Left"
                self.INFO.toSwitchJustOnce = (pieceIndex + 1)%totalPieces
                logging.info ("Left")
        else:
            return "Same"

    def shouldUseTurbo(self):
        if (self.INFO.myCar.pieceIndex == self.INFO.turboIndex and self.INFO.turboAvailable):
            return True
        else:
            return False

