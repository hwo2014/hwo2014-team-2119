import json
import socket
import sys
import logging
import signal
import sys
from information import *
from think import *
from logger import *

INFO = Information()
THINK = Think(INFO)
LOGGER = Logger(INFO, THINK)

class NoobBot(object):
    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        return self.msg("join", {"name":self.name, "key": self.key})
        # return self.msg("createRace", {"botId":{"name":self.name, "key":self.key }, "trackName":"france", "password":"senha", "carCount":1 })

    def throttle(self, throttle):
        #self.msg("throttle", throttle)
        #self.send({"msgType":"throttle", "data":throttle, "gameTick":INFO.gameTick})
        self.send(json.dumps({"msgType": "throttle", "data": throttle, "gameTick":INFO.gameTick}))

    def switch_lane(self, side): 
        print "switch lanes" 
        logging.info("switch lane")
        self.msg("switchLane", side)

    def turbo (self, data):
        self.msg("turbo", data)
        INFO.turboAvailable = False

    def ping(self):
        self.msg("ping", {})
        
    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print "Joined"
        logging.info("Joined")
        INFO.init_my_car_info( data )
        self.ping()
        
    def on_join_my_car(self, data):
        print "Joined"
        logging.info("Joined")
        newData = { "name":data["name"], "key":self.key }
        INFO.init_my_car_info( newData )
        self.ping()

    def on_game_start(self, data):
        print "Race started"
        logging.info("Race started") 
        self.ping()
        
    def on_game_init( self, data ):
        print "GameInit"
        INFO.get_info(data)
        self.ping()

    def on_car_positions(self, data):
        INFO.update_my_car_info(data)
        THINK.calculateThrottle()
        INFO.update_my_car_throttle(THINK.getThrottle())
        INFO.update_my_car_power()
        
        #logica
        if( INFO.gameTick == 0 ):
            self.throttle(1.0)
        #switch lane            
        elif (THINK.getBestLane() != "Same" and (not INFO.changeSwitch) ):
            if( (THINK.getBestLane() == "Right" and INFO.myCar.endLaneIndex + 1 < len(INFO.distanceFromCenter)) or (THINK.getBestLane() == "Left" and INFO.myCar.endLaneIndex - 1 >= 0 ) ):
                INFO.changeSwitch =  True
                self.switch_lane(THINK.getBestLane())
            else:
                self.throttle(THINK.getThrottle())
        #turbo
        else:
            if THINK.shouldUseTurbo():
                self.turbo("Turbo!!!!")
            else:
                self.throttle(THINK.getThrottle())
                
        LOGGER.mainLog()

    def on_crash(self, data):
        print "Y O U   C R A S H E D ! ! !   Y O U   A R E   N O O B ! ! !"
        logging.info("Someone crashed")
        INFO.update_crash()
        self.ping()

    def on_game_end(self, data):
        logging.info("Race ended")
        self.ping()

    def on_error(self, data):
        logging.info("Error: {0}".format(data))
        self.ping()

    def on_turbo_availability(self, data):
        INFO.turboAvailable = True
        INFO.turboFactorForNextTurbo = data["turboFactor"]
        logging.info("Turbo Available")
        
    def on_turbo_start(self, data):
        INFO.turboFactor = INFO.turboFactorForNextTurbo
        INFO.update_my_car_power()
        print "S O L T A R   T U R B O ! ! !"
        
    def on_turbo_end( self, data ):
        INFO.turboFactor = 1.0
        INFO.update_my_car_power()
        INFO.turboAvailable = False

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'gameInit': self.on_game_init,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'turboAvailable': self.on_turbo_availability,
            'yourCar': self.on_join_my_car,
            'turboStart': self.on_turbo_start,
            'turboEnd': self.on_turbo_end
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                if msg_type == 'carPositions' and ( 'gameTick' in msg ):
                    INFO.update_game_tick( msg['gameTick'] )
                msg_map[msg_type](data)
            else:
                logging.info("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()

if __name__ == "__main__":
    if len(sys.argv) != 5:
        Logger.commandLineError()
    else:
        host, port, name, key = sys.argv[1:5]
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
    logging.info('Finished')
