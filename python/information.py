from math import pi
import json
import math
import logging

class Piece(object):
    length = 0
    switch = False
    radius = 0
    angle = 0
    originalRadius = 0
    originalLength = 0

    def __init__(self, length=0.0, switch=0.0, radius=0.0, angle=0.0):
        self.length = length
        self.orinalLength = length
        self.switch = switch
        self.originalRadius = radius

class Car:
    Racing = False
    name = "nullName"
    key = "nullKey"
    color = "nullColor"
    angle = 0.0
    lastPieceIndex = 0
    pieceIndex = 0
    lastInPieceDistance = 0.0
    inPieceDistance = 0.0
    startLaneIndex = 0
    endLaneIndex = 0
    lap = 0
    lastVelocity = 0.0
    velocity = 0.0
    lastAceleration = 0.0
    aceleration = 0.0
    throttle = 0.0
    power = 0.0

class Information:
    myCar = Car()
    lastTurbo = 0
    totalPieces = 0
    gameTick = 0
    TrackList = []
    distanceFromCenter = []
    turboAvailable = False 
    friction = 0.0
    findCtes = False
    absurdAceleration = False
    turboAvailable = False
    countLength = 0
    sum = 0
    sum2 = 0
    indexToSwitch = -1
    rightOrLeft = []
    toSwitchJustOnce = 100000
    switchLane = False
    turboFactor = 1.0
    turboFactorForNextTurbo = 1.0
    nSwitchLane = 0
    changeSwitch = False
    turboIndex = 0

    def get_info (self, data):
        #Moller
        self.rightOrLeft = [0]*len(data["race"]["track"]["pieces"])
        self.toSwitchJustOnce = len(data["race"]["track"]["pieces"]) + 1
        self.myCar.Racing = True

        for i, p in enumerate(data["race"]["track"]["pieces"]):
            piece = Piece()
            if( "length" in p ):
                logging.info(p)
                piece.length = p["length"]
                piece.originalLength = p["length"]
                piece.originalRadius = 0
                piece.angle = 0
            else:
                piece.originalRadius = p["radius"]
                piece.angle = p["angle"]
                #moller
                if self.indexToSwitch != -1:
                    self.sum = self.sum + piece.originalRadius * piece.angle
                else:
                    self.sum2 = self.sum2 + piece.originalRadius * piece.angle
            
            if( "switch" in p ):
                piece.switch = True
                #moller
                if self.indexToSwitch != -1:
                    self.rightOrLeft[self.indexToSwitch] = self.sum
                    self.sum = 0
                self.indexToSwitch = i
            else:
                piece.switch = False
            self.TrackList.append(piece)
        #moller
        if self.indexToSwitch != -1:
            self.rightOrLeft[self.indexToSwitch] = self.sum + self.sum2

        for lane in data["race"]["track"]["lanes"]:
            self.distanceFromCenter.append(lane["distanceFromCenter"])
        
        self.totalPieces = len(self.TrackList)
        self.get_turbo_index_piece()


    def init_my_car_info(self, data):
        self.myCar.name = data["name"]
        self.myCar.key = data["key"]
            
    def update_my_car_info(self, data):    
        self.myCar.lastVelocity = self.myCar.velocity
        self.myCar.lastAceleration = self.myCar.aceleration
        self.myCar.lastPieceIndex = self.myCar.pieceIndex
        self.myCar.lastInPieceDistance = self.myCar.inPieceDistance

        for carInfo in data :
            if carInfo["id"]["name"] == self.myCar.name:
                self.myCar.angle = carInfo[ "angle"]
                self.myCar.pieceIndex = carInfo["piecePosition"]["pieceIndex"]
                self.myCar.inPieceDistance = carInfo["piecePosition"]["inPieceDistance"]
                self.myCar.startLaneIndex = carInfo["piecePosition"]["lane"]["startLaneIndex"]
                self.myCar.endLaneIndex = carInfo["piecePosition"]["lane"]["endLaneIndex"]
                self.myCar.lap = carInfo["piecePosition"]["lap"]
        
        #atualiza antes de iniciar a corrida (gameTick == 0) os raios e lenghts das curvas
        #imprime a configuracao da pista baseada no meu lane atual
        if( self.gameTick == 0 ):
            for piece in self.TrackList:
                if( piece.originalRadius != 0 ):
                    if(piece.angle > 0):
                        piece.radius = piece.originalRadius - self.distanceFromCenter[self.myCar.startLaneIndex]
                    else:
                        piece.radius = piece.originalRadius + self.distanceFromCenter[self.myCar.startLaneIndex]
                    piece.length = abs((pi * piece.angle / 180.0 ) * piece.radius)
                    piece.originalLength = abs((pi * piece.angle / 180.0 ) * piece.originalRadius)
                    

            print "*********************** CONFIGURACAO DO CIRCUITO ***********************"
            aux = 0
            for piece in self.TrackList:
                print ("index = " + str(aux) + " length = " + str(piece.length) + " switch " + str(piece.switch) + " radius " + str(piece.radius) + " angle " + str(piece.angle) )
                logging.info ("index = " + str(aux) + " length = " + str(piece.length) + " switch " + str(piece.switch) + " radius " + str(piece.radius) + " angle " + str(piece.angle) )
                #moller
                if self.rightOrLeft[aux] != 0:
                    logging.info(self.rightOrLeft[aux])
                aux = aux + 1
                
        #calculo velocidade        
        if self.myCar.pieceIndex == self.myCar.lastPieceIndex:
            self.myCar.velocity = self.myCar.inPieceDistance - self.myCar.lastInPieceDistance
        else:
            logging.info ("trocou de Piece")
            print "*********************** TROCOU PIECE *********************** RADIUS = " + str(self.TrackList[self.myCar.pieceIndex].radius)
            self.myCar.velocity = self.TrackList[self.myCar.lastPieceIndex].length-self.myCar.lastInPieceDistance + self.myCar.inPieceDistance
        
        if not self.absurdAceleration:
            self.myCar.aceleration = self.myCar.velocity - self.myCar.lastVelocity
        else:
            self.myCar.aceleration = (self.myCar.velocity - self.myCar.lastVelocity)/2
            self.absurdAceleration = False
        
        if self.myCar.aceleration > 0 and (not self.findCtes):
            self.friction = self.myCar.aceleration/(10*1-self.myCar.velocity)
            self.myCar.power = 10*self.myCar.throttle*self.friction
            print "friction = " + str(self.friction)
            logging.info("friction = " + str(self.friction))
            logging.info("carPower = " + str(self.myCar.power))
            self.findCtes = True
        
        maxi = self.myCar.power
        if 10*self.friction > maxi:
            maxi = 10*self.friction
        maxAceleration = maxi + 1
        
        if math.fabs(self.myCar.aceleration) > maxAceleration:
            self.absurdAceleration = True
            
        #caso ocorreu a troca de lanes, atualiza os lenghts e raios das curvas
        #tb atualiza o lenght da reta em que ocorre a troca de lane
        if( self.myCar.endLaneIndex != self.myCar.startLaneIndex and (not self.switchLane) ):
            self.switchLane = True
            self.TrackList[self.myCar.pieceIndex].length = math.sqrt((self.TrackList[self.myCar.pieceIndex].originalLength**2)+(math.fabs(self.distanceFromCenter[self.myCar.endLaneIndex]-self.distanceFromCenter[self.myCar.startLaneIndex])**2))
            aux = 0
            for piece in self.TrackList:
                if( piece.originalRadius != 0 and aux != self.myCar.pieceIndex ):
                    if(piece.angle > 0):
                        piece.radius = piece.originalRadius - self.distanceFromCenter[self.myCar.endLaneIndex]
                    else:
                        piece.radius = piece.originalRadius + self.distanceFromCenter[self.myCar.endLaneIndex]
                    piece.length = abs((pi * piece.angle / 180.0 ) * piece.radius)
                aux = aux+1
            
        #apos trocar de lane, o valor do length onde a troca de lane foi feita volta a seu valor original
        if(self.switchLane and self.myCar.endLaneIndex == self.myCar.startLaneIndex):
            self.changeSwitch = False
            #test
            print "mudou de lane no piece anterior"
            self.switchLane = False
            if( self.TrackList[self.myCar.lastPieceIndex].radius == 0 ):
                self.TrackList[self.myCar.lastPieceIndex].length = self.TrackList[self.myCar.lastPieceIndex].originalLength
            else:
                if(self.TrackList[self.myCar.lastPieceIndex].angle > 0):
                    self.TrackList[self.myCar.lastPieceIndex].radius = self.TrackList[self.myCar.lastPieceIndex].originalRadius - self.distanceFromCenter[self.myCar.startLaneIndex]
                else:
                    self.TrackList[self.myCar.lastPieceIndex].radius = self.TrackList[self.myCar.lastPieceIndex].originalRadius + self.distanceFromCenter[self.myCar.startLaneIndex]
                self.TrackList[self.myCar.lastPieceIndex].length = abs(pi * self.TrackList[self.myCar.lastPieceIndex].angle / 180.0 )*self.TrackList[self.myCar.lastPieceIndex].radius
    
    def update_game_tick( self, gameTickReceived ):
        self.gameTick = gameTickReceived
        
    def update_my_car_throttle(self, throttleReceived):
        self.myCar.throttle = throttleReceived
        
    def update_my_car_power(self):
        self.myCar.power = 10*self.myCar.throttle*self.friction*self.turboFactor
        
    def update_crash(self):
        self.myCar.velocity, self.myCar.lastVelocity = 0.0, 0.0
        self.myCar.aceleration, self.myCar.lastAceleration = 0.0, 0.0
    
    def get_last_not_curve_index (self, index = 0.0):
    	if (index == 0.0):
        	indAtu = self.myCar.pieceIndex
       	else:
       		indAtu = index
        for i in range (indAtu + 1, indAtu + self.totalPieces):
            if self.TrackList[i%(self.totalPieces)].originalRadius != 0 and self.TrackList[i%(self.totalPieces)].originalRadius != self.TrackList[indAtu].originalRadius:
                return (i + self.totalPieces - 1)%(self.totalPieces)

    def get_turbo_index_piece(self):
    	self.turboIndex = 0;
    	maxDist = 0
    	for index, piece in enumerate(self.TrackList):
    		if (piece.originalRadius == 0.0 and (self.get_last_not_curve_index(index) - index + self.totalPieces)%self.totalPieces > maxDist):
    			self.turboIndex = index
    			maxDist = (self.get_last_not_curve_index(index) - index + self.totalPieces)%self.totalPieces 

    	# print "MELHOR INDICE PARA USAR TURBO = " + str(self.turboIndex)
